import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import SearchPokemon from "./components/SearchPage";
import MovesPage from "./components/MovePage";
import TypesPage from "./components/TypePage";
import GenerationOne from "./components/Gen1Page";
import GenerationTwo from "./components/Gen2Page";
import GenerationThree from "./components/Gen3Page";
import GenerationFour from "./components/Gen4Page";
import GenerationFive from "./components/Gen5Page";
import NavBar from "./components/Navbar";

function App() {
  return (
    <BrowserRouter>
      <>
        <NavBar />
        <Switch>
          <Route exact path={"/"} component={SearchPokemon}></Route>
          <Route exact path={"/movesPage"} component={MovesPage}></Route>
          <Route exact path={"/typePage"} component={TypesPage}></Route>
          <Route exact path={"/gen1"} component={GenerationOne}></Route>
          <Route exact path={"/gen2"} component={GenerationTwo}></Route>
          <Route exact path={"/gen3"} component={GenerationThree}></Route>
          <Route exact path={"/gen4"} component={GenerationFour}></Route>
          <Route exact path={"/gen5"} component={GenerationFive}></Route>
        </Switch>
      </>
    </BrowserRouter>
  );
}

export default App;
