import React, { useState } from "react";
import { Link } from "react-router-dom";
import Axios from "axios";
import "../App.css";

export default function TypesPage() {
  const [pokemonType, setPokemonType] = useState();
  const [pokemon, setPokemon] = useState({
    name: "",
    number: "",
    image: "",
    moves: "",
  });

  const searchPokemon = () => {
    Axios.get(`https://pokeapi.co/api/v2/pokemon/type/${pokemonType}`).then(
      (res) => {
        setPokemon({
          image: res.data.sprites.front_default,
          name: res.data.name,
        });
      }
    );
  };

  return (
    <div className="App">
      <div className="TitleSection">
        <h1>Check Pokemon by types</h1>
        <div>
          <button onClick={searchPokemon}>{setPokemonType(0)}</button>
        </div>
        <div>
          <button onClick={searchPokemon}>{setPokemonType(1)}</button>
        </div>
        <div>
          <button onClick={searchPokemon}>{setPokemonType(2)}</button>
        </div>
      </div>
      <div className="box">
        <div className="DisplaySection">
          <img src={pokemon.image} alt={pokemon.name} />
          <h1>{pokemon.name}</h1>
        </div>
        <Link to={"/"}>
          <button className="pretty-button">Home</button>
        </Link>
      </div>
    </div>
  );
}
