import React from "react";
import "../App.css";

export default function PokemonDisplay({ id, image, name }) {
  return (
    <div className="poke-container">
      <div>
        <p>#{id}</p>
      </div>
      <img src={image} alt={name} width={80} height={80} />
      <div className="wrapper">
        <h3>{name.toLowerCase()}</h3>
      </div>
    </div>
  );
}
