import React, { useState, useEffect } from "react";
import "./pokeStyles/PokeDisplayStyles.css";
import PokemonDisplay from "./DisplayPoke";
export default function GenerationFive() {
  const [pokemon, setPokemon] = useState([]);
  const loadPoke = `https://pokeapi.co/api/v2/pokemon?limit=156&offset=493`;

  const getfifthGen = async () => {
    const res = await fetch(loadPoke);
    const data = await res.json();

    function getPokemonObject(object) {
      object.forEach(async (poke) => {
        const res = await fetch(
          `https://pokeapi.co/api/v2/pokemon/${poke.name}`
        );
        const data = await res.json();

        setPokemon((currentArray) =>
          [...currentArray, data].sort((a, b) => (a.id > b.id ? 1 : -1))
        );
      });
    }
    getPokemonObject(data.results);
  };

  useEffect(() => {
    getfifthGen();
  }, []);

  return (
    <div className="App">
      <header>
        <div></div>
      </header>
      <div className="sep-pokes">
        {pokemon.map((pokemon, index) => (
          <PokemonDisplay
            key={index}
            id={pokemon.id}
            image={pokemon.sprites.front_default}
            name={pokemon.name}
          />
        ))}
      </div>
    </div>
  );
}
