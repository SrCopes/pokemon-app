/* eslint-disable no-const-assign */
import React, { useState, useEffect } from "react";
import "./pokeStyles/PokeDisplayStyles.css";
import PokemonDisplay from "./DisplayPoke";
export default function GenerationOne() {
  const [pokemon, setPokemon] = useState([]);
  const loadPoke = `https://pokeapi.co/api/v2/pokemon?limit=151&offset=0`;

  const getfirstGen = async () => {
    const res = await fetch(loadPoke);
    const data = await res.json();

    function getPokemonObject(results) {
      results.forEach(async (poke) => {
        const res = await fetch(
          `https://pokeapi.co/api/v2/pokemon/${poke.name}`
        );
        const data = await res.json();

        setPokemon((currentArray) =>
          [...currentArray, data].sort((a, b) => (a.id > b.id ? 1 : -1))
        );
      });
    }
    getPokemonObject(data.results);
  };

  useEffect(() => {
    getfirstGen();
  }, []);

  return (
    <div className="App">
      <div className="sep-pokes">
        {pokemon.map((pokemon, index) => (
          <PokemonDisplay
            key={index}
            id={pokemon.id}
            image={pokemon.sprites.front_default}
            name={pokemon.name}
          />
        ))}
      </div>
    </div>
  );
}
