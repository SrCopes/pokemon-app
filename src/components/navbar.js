import React from "react";
import { NavLink } from "react-router-dom";
import "./pokeStyles/Navbar.css";

export default function NavBar() {
  return (
    <navbar className="navbar">
      <NavLink to={"/"}>Home</NavLink>
      <div className="dropdown">
        <button className="dropbtn">
          Gens (I - V)
          <i className="fa fa-caret-down"></i>
        </button>
        <div className="dropdown-content">
          <NavLink to={"/gen1"}>Gen I</NavLink>
          <NavLink to={"/gen2"}>Gen II</NavLink>
          <NavLink to={"/gen3"}>Gen III</NavLink>
          <NavLink to={"/gen4"}>Gen IV</NavLink>
          <NavLink to={"/gen5"}>Gen V</NavLink>
        </div>
      </div>
      <NavLink to={"/movesPage"}>Moves Page</NavLink>
    </navbar>
  );
}
