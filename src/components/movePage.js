import React, { useState } from "react";
import Axios from "axios";
import "../App.css";
import "./pokeStyles/Search&Moves.css";

export default function MovesPage() {
  const [pokemonName, setPokemonName] = useState("");
  const [pokemon, setPokemon] = useState({
    name: "",
    number: "",
    image: "",
    moves: "",
  });
  const [pokemonChosen, setPokemonChosen] = useState(false);

  const searchPokemon = () => {
    Axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`).then(
      (res) => {
        setPokemon({
          name: pokemonName,
          number: res.data.id,
          image: res.data.sprites.other.home.front_default,
          moves: res.data.moves.map((item) => " " + item.move.name + " "),
        });
        setPokemonChosen(true);
      }
    );
  };

  return (
    <div className="App">
      <div className="TitleSection">
        <h1>Check Pokemon Moves</h1>
        <input
          type="text"
          onChange={(event) => {
            setPokemonName(event.target.value);
          }}
          value={pokemonName.toLowerCase()}
        />
        <div>
          <button onClick={searchPokemon}>Type the name</button>
        </div>
      </div>
      <div className="box">
        <div className="DisplaySection">
          {!pokemonChosen ? (
            <h1> Type a pokemon to see the move </h1>
          ) : (
            <>
              <h1>{pokemon.name}</h1>

              <img src={pokemon.image} alt={pokemon.name} />
              <h3>
                <strong>Number:</strong> #{pokemon.number}
              </h3>
              <p>
                <h2>Moves:</h2> {pokemon.moves}
              </p>
            </>
          )}
        </div>
      </div>
    </div>
  );
}
